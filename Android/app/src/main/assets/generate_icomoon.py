import re;
from json import JSONEncoder;

def write_resources(file, icons):
	file.write('<?xml version="1.0" encoding="utf-8"?>\n');
	file.write('<resources>\n');
	icons_dict = {};
	for icon in icons:
		file.write('\t<string name="icomoon_%s">&#x%s;</string>\n' % icon);
		icons_dict[icon[0]] = int(icon[1], 16);
	json_string = JSONEncoder(separators=(',',':')).encode(icons_dict);
	file.write('\n\t<string name="icomoon_mapping">%s</string>\n' % json_string);
	file.write('</resources>\n');

if __name__ == '__main__':
	style_file = open('style.css', 'r');
	style = style_file.read();
	style = style.replace('-', '_');
	icon_pattern = re.compile(r'\.icon_([\w_]+):before\s*{[\s\n\r]*content:\s*\"\\([\w\d]+)');
	icons = icon_pattern.findall(style);
	
	icomoon = open('icomoon.xml', 'w');
	write_resources(icomoon, icons);
	