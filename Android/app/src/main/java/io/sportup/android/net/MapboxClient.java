package io.sportup.android.net;

import android.content.res.Resources;
import android.net.Uri;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.sportup.android.R;
import io.sportup.android.map.Place;

public class MapboxClient extends AsyncHttpClient {

    private static final String TAG = "net.MapboxClient";

    private final Resources mResources;
    private final Uri mBaseUri;

    public MapboxClient(Resources resources) {
        mResources = resources;

        Uri.Builder builder = new Uri.Builder();
        builder.scheme(getString(R.string.mapbox_api_scheme))
                .authority(getString(R.string.mapbox_api_host))
                .appendPath(getString(R.string.mapbox_api_version));
        mBaseUri = builder.build();
    }

    // e.g. http://api.tiles.mapbox.com/v4/geocode/mapbox.places/{query}.json?access_token=<your access token>
    public void places(String query, final PlaceListHandler responseHandler) {
        Uri.Builder builder = mBaseUri.buildUpon();
        builder.appendPath(getString(R.string.mapbox_api_geocode));
        builder.appendPath(getString(R.string.mapbox_api_places));
        builder.appendPath(query + ".json");
        builder.appendQueryParameter(getString(R.string.mapbox_api_token_key),
                getString(R.string.mapbox_api_token));

        Log.d(TAG, "Search places: " + builder.build().toString());
        get(builder.build().toString(), new JsonResponseHandler() {
            @Override
            public void success(int status, JSONObject response) {
                List<Place> places = new ArrayList<Place>();
                try {
                    JSONArray features = response.getJSONArray("features");
                    for(int i = 0; i < features.length(); ++i) {
                        JSONObject feature = features.getJSONObject(i);
                        places.add(new Place(feature));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                responseHandler.handle(places);
            }

            @Override
            public void failure(int status, JSONObject response) {}
        });
    }

    private String getString(int resourceId) {
        return mResources.getString(resourceId);
    }

    public interface PlaceListHandler {
        void handle(List<Place> places);
    }
}
