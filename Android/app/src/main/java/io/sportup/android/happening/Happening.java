package io.sportup.android.happening;

import com.mapbox.mapboxsdk.api.ILatLng;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

import io.sportup.android.utils.GeoUtils;

public class Happening {

    private static final String TAG = "Happening";

    private String mDescription;
    private ILatLng mPosition;
    private String mActivity;
    private Date mDateTime;

    public void setDescription(String description) {
        mDescription = description;
    }

    public void setPosition(ILatLng position) {
        mPosition = position;
    }

    public void setActivity(String activity) {
        mActivity = activity;
    }

    public void setDateTime(Date dateTime) {
        mDateTime = dateTime;
    }

    public JSONObject toJson() {
        JSONObject data = new JSONObject();
        try {
            data.put("date", mDateTime);
            data.put("activity", mActivity);
            data.put("description", mDescription);
            data.put("loc", GeoUtils.createLoc(mPosition));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return data;
    }
}
