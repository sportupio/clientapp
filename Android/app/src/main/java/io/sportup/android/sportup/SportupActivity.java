package io.sportup.android.sportup;

import android.app.*;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;

import java.util.ArrayList;

import io.sportup.android.R;
import io.sportup.android.localization.CreateActivity;
import io.sportup.android.sportup.stream.Discover;
import io.sportup.android.sportup.stream.Events;
import io.sportup.android.sportup.stream.Home;
import io.sportup.android.utils.Icomoon;
import io.sportup.android.utils.OnSwipeTouchListener;


public class SportupActivity extends Activity {

    private static final String TAG = "SportupActivity";

    private TabHost mTabHost;
    private TabWidget mTabWidget;
    public static final String TAB_REGISTER = "TAB_REGISTER";
    private ArrayList<Fragment> mFragmentList;
    private int mCurrentFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sportup_activity);

        mCurrentFragment = 1;
        mFragmentList = new ArrayList<Fragment>();
        mFragmentList.add(new Events());
        mFragmentList.add(new Home());
        mFragmentList.add(new Discover());
        switchFragment(0);

        attachSwipeListener();

        Icomoon.set((TextView) findViewById(R.id.button_map));
    }

    @Override
    public boolean onNavigateUp() {
        // Should we close application here ?
        finish();
        return true;
    }

    private void switchFragment(int direction) {
        mCurrentFragment = (mCurrentFragment + mFragmentList.size() + direction) % mFragmentList.size();
        Log.d(TAG, "CurrentFragment: " + mCurrentFragment);

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        if(direction > 0) {
            ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
        } else if (direction < 0) {
            ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
        }
        ft.replace(R.id.main_fragment, mFragmentList.get(mCurrentFragment))
                .addToBackStack(null)
                .commit();
    }

    private void attachSwipeListener() {
        View fragment = findViewById(R.id.main_fragment);
        fragment.setOnTouchListener(new OnSwipeTouchListener(getApplicationContext()) {
            @Override
            public void onSwipeRight() {
                Log.d(TAG, "onSwipeRight");
                switchFragment(-1);
            }

            @Override
            public void onSwipeLeft() {
                Log.d(TAG, "onSwipeLeft");
                switchFragment(1);
            }

            @Override
            public void onSwipeTop() {
                Log.d(TAG, "onSwipeTop");
            }

            @Override
            public void onSwipeBottom() {
                Log.d(TAG, "onSwipeBottom");
            }
        });
    }

    @Override
    public void onBackPressed() {
        getFragmentManager().popBackStackImmediate(null,FragmentManager.POP_BACK_STACK_INCLUSIVE);
        super.onBackPressed();
    }

    public void createHappening(View view) {
        Intent intent = new Intent(getApplicationContext(), io.sportup.android.happening.CreateActivity.class);
        startActivity(intent);
    }

    public void createLocalization(View view) {
        Intent intent = new Intent(getApplicationContext(), io.sportup.android.localization.CreateActivity.class);
        startActivity(intent);
    }
}
