package io.sportup.android.sportup;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONArray;
import org.json.JSONObject;

import io.sportup.android.Application;
import io.sportup.android.R;
import io.sportup.android.net.ApiClient;

public class UserStreamActivity extends Activity {

    private static final String TAG = "UserStreamActivity";
    private ApiClient mApi;
    private Socket mSocket;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sportup_activity_user_stream);
        mApi = ((Application) getApplication()).api();

        // Socket initialization can be done only after we have a token
        mSocket = ((Application) getApplication()).initializeSocket();

        mSocket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.d(TAG, "Socket connection established");
            }
        }).on("userStream:main", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.d(TAG, "UserStream:main event received");
                Object obj = args[0];
                if(obj instanceof JSONArray) {
                    JSONArray array = (JSONArray)obj;
                    Log.d(TAG, "Received array data: " + array.toString());
                } else if (obj instanceof JSONObject) {
                    JSONObject jobj = (JSONObject)obj;
                    Log.d(TAG, "Received object data: " + jobj.toString());
                }

            }
        });
        mSocket.connect();
    }
}
