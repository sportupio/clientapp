package io.sportup.android.auth;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import io.sportup.android.R;

public class DeleteAccount extends Fragment {

    private static final String TAG = "auth.DeleteAccount";

    public DeleteAccount() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.auth_help_delete_account, container, false);
    }
}
