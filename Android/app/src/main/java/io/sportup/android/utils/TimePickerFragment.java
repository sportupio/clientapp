package io.sportup.android.utils;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.widget.TimePicker;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Konrad on 2015-05-02.
 */
public class TimePickerFragment extends DialogFragment {

    TimePickerDialog.OnTimeSetListener mListener;

    public static TimePickerFragment newInstance(Calendar calendar, TimePickerDialog.OnTimeSetListener listener) {
        TimePickerFragment fragment = new TimePickerFragment();

        fragment.mListener = listener;
        Bundle args = new Bundle();
        args.putSerializable("calendar", calendar);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Calendar calendar = (Calendar) getArguments().getSerializable("calendar");

        // Create a new instance of TimePickerDialog and return it
        return new TimePickerDialog(getActivity(), mListener, calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE), DateFormat.is24HourFormat(getActivity()));
    }
}
