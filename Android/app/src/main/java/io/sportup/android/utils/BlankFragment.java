package io.sportup.android.utils;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import io.sportup.android.R;

public class BlankFragment extends Fragment {

    private static final String TAG = "utils.BlankFragment";

    public BlankFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.sportup_utils_blank_fragment, container, false);
    }

}
