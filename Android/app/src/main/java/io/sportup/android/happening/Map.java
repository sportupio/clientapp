package io.sportup.android.happening;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.SeekBar;

import com.mapbox.mapboxsdk.api.ILatLng;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.overlay.Icon;
import com.mapbox.mapboxsdk.overlay.Marker;
import com.mapbox.mapboxsdk.tileprovider.tilesource.MapboxTileLayer;
import com.mapbox.mapboxsdk.views.MapView;
import com.mapbox.mapboxsdk.views.MapViewListener;

import io.sportup.android.R;
import io.sportup.android.map.BasicFragment;
import io.sportup.android.map.DraggableMarker;

public class Map extends BasicFragment {

    private static final String TAG = "happening.Map";

    private DraggableMarker mMarker;

    public Map() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        LatLng aCenter = new LatLng(43.732654, 7.421951);
        mMapView.setCenter(aCenter);

        mMarker = new DraggableMarker(mMapView, "", "", aCenter);
        mMarker.setIcon(new Icon(getActivity().getApplicationContext(), Icon.Size.SMALL, "marker-stroked", "FF0000"));
        mMapView.addMarker(mMarker);

        mMapView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return mMarker.drag(v, event);
            }
        });

        mMapView.setMapViewListener(new MapViewListener() {
            @Override
            public void onShowMarker(MapView mapView, Marker marker) {

            }

            @Override
            public void onHideMarker(MapView mapView, Marker marker) {

            }

            @Override
            public void onTapMarker(MapView mapView, Marker marker) {
                Log.d(TAG, "onTapMarker");
            }

            @Override
            public void onLongPressMarker(MapView mapView, Marker marker) {
                Log.d(TAG, "onLongPressMarker");
            }

            @Override
            public void onTapMap(MapView mapView, ILatLng iLatLng) {
                Log.d(TAG, "Tap event");
                LatLng pos = new LatLng(iLatLng.getLatitude(), iLatLng.getLongitude(), iLatLng.getAltitude());
                mMarker.setPoint(pos);
            }

            @Override
            public void onLongPressMap(MapView mapView, ILatLng iLatLng) {

            }
        });

        return view;
    }

    public void submit(Happening happening) {
        happening.setPosition(mMarker.getPoint());
    }
}
