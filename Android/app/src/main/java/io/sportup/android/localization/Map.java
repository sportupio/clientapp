package io.sportup.android.localization;

import android.os.Bundle;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.SeekBar;

import com.mapbox.mapboxsdk.api.ILatLng;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.overlay.Icon;
import com.mapbox.mapboxsdk.overlay.Marker;
import com.mapbox.mapboxsdk.views.MapView;
import com.mapbox.mapboxsdk.views.MapViewListener;

import io.sportup.android.R;
import io.sportup.android.map.BasicFragment;
import io.sportup.android.map.CircleOverlay;
import io.sportup.android.map.DraggableMarker;

public class Map extends BasicFragment {

    private static final String TAG = "user_area.Map";

    private DraggableMarker mMarker;
    private SeekBar mRadius;
    private CircleOverlay mArea;

    public Map() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        LatLng aCenter = new LatLng(43.732654, 7.421951);
        mArea = new CircleOverlay(20.0, aCenter);
        setupRadiusSlider(view);

        mMapView.setCenter(aCenter);

        mMarker = new DraggableMarker(mMapView, "", "", aCenter);
        mMarker.setIcon(new Icon(getActivity().getApplicationContext(), Icon.Size.SMALL, "marker-stroked", "FF0000"));
        mMapView.addMarker(mMarker);
        mMapView.addOverlay(mArea);
        mArea.addTo(mMapView);

        mMapView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(mMarker.drag(v, event)) {
                    mArea.setCenter(mMarker.getPoint());
                    return true;
                }
                return false;
            }
        });

        mMapView.setMapViewListener(new MapViewListener() {
            @Override
            public void onShowMarker(MapView mapView, Marker marker) {

            }

            @Override
            public void onHideMarker(MapView mapView, Marker marker) {

            }

            @Override
            public void onTapMarker(MapView mapView, Marker marker) {
                Log.d(TAG, "onTapMarker");
            }

            @Override
            public void onLongPressMarker(MapView mapView, Marker marker) {
                Log.d(TAG, "onLongPressMarker");
            }

            @Override
            public void onTapMap(MapView mapView, ILatLng iLatLng) {
                Log.d(TAG, "Tap event");
                LatLng pos = new LatLng(iLatLng.getLatitude(), iLatLng.getLongitude(), iLatLng.getAltitude());
                mMarker.setPoint(pos);
                mArea.setCenter(pos);
            }

            @Override
            public void onLongPressMap(MapView mapView, ILatLng iLatLng) {

            }
        });

        return view;
    }

    private void setupRadiusSlider(View view) {
        mRadius = new SeekBar(new ContextThemeWrapper(getActivity().getBaseContext(), R.style.sportup_io_map_style));
        mRadius.setMax(100);
        mRadius.setProgress(progress(20.0));

        LinearLayout containerLayout = (LinearLayout)view.findViewById(R.id.layout_map_bottom);
        containerLayout.addView(mRadius,
                new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT));

        mRadius.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                Log.d(TAG, "Radius changed: " + radius(progress));
                mArea.setRadius(radius(progress));
                mMapView.invalidate();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    public ILatLng getMarkerPosition() {
        return mMarker.getPoint();
    }

    private double radius(int progress) {
        return (1 + (49 * progress / 100));
    }

    private int progress(double radius) {
        return (int)((radius - 1) * 100/49);
    }

    public void submit(Localization localization) {
        localization.setRadius(mArea.radius());
        localization.setPosition(mMarker.getPoint());
    }
}
