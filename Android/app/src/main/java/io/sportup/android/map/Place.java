package io.sportup.android.map;

import android.util.Log;

import com.mapbox.mapboxsdk.geometry.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Place implements CharSequence {

    private static final String TAG = "map.Place";

    public final LatLng mLoc;
    public final String mName;

    public Place(JSONObject json) throws JSONException {
        JSONArray center = json.getJSONArray("center");
        mLoc = new LatLng(center.getDouble(1), center.getDouble(0));
        mName = json.getString("place_name");

        Log.d(TAG, "Created place: " + mName);
    }

    @Override
    public int length() {
        return 0;
    }

    @Override
    public char charAt(int index) {
        return 0;
    }

    @Override
    public CharSequence subSequence(int start, int end) {
        return null;
    }

    @Override
    public String toString() {
        return mName;
    }
}
