package io.sportup.android.utils;

import com.mapbox.mapboxsdk.api.ILatLng;

import org.json.JSONArray;
import org.json.JSONException;

public class GeoUtils {

    public static JSONArray createLoc(ILatLng pos) throws JSONException {
        JSONArray loc = new JSONArray();
        loc.put(pos.getLongitude());
        loc.put(pos.getLatitude());
        return loc;
    }
}
