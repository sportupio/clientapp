package io.sportup.android.happening;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import io.sportup.android.R;
import io.sportup.android.utils.DatePickerFragment;
import io.sportup.android.utils.Icomoon;
import io.sportup.android.utils.TimePickerFragment;

public class Form extends Fragment {

    private static final String TAG = "happening.CreateForm";

    private Calendar mCalendar;
    private TextView mDateTimeView;

    public Form() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.happening_form_fragment, container, false);

        setupActivitySpinner(view);
        setupDateTimePicker(view);
        return view;
    }

    void setupDateTimePicker(View view) {
        Button dateBtn = (Button)view.findViewById(R.id.button_date);
        Button timeBtn = (Button)view.findViewById(R.id.button_time);
        Icomoon.set((TextView) view.findViewById(R.id.button_date));
        Icomoon.set((TextView) view.findViewById(R.id.button_time));

        mDateTimeView = (TextView)view.findViewById(R.id.output_date_time);
        mCalendar = Calendar.getInstance();

        mCalendar.set(Calendar.HOUR_OF_DAY, 12);
        mCalendar.set(Calendar.MINUTE, 0);
        mCalendar.add(Calendar.DATE, 1);
        updateDateTime();

        dateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerFragment newFragment = DatePickerFragment.newInstance(mCalendar,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int month, int day) {
                                mCalendar.set(year, month, day);
                                updateDateTime();
                            }
                        });
                newFragment.show(getFragmentManager(), "datePicker");
            }
        });
        timeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimePickerFragment newFragment = TimePickerFragment.newInstance(mCalendar,
                        new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker view, int hour, int minute) {
                                mCalendar.set(Calendar.HOUR_OF_DAY, hour);
                                mCalendar.set(Calendar.MINUTE, minute);
                                updateDateTime();
                            }
                        });
                newFragment.show(getFragmentManager(), "timePicker");
            }
        });
    }

    void setupActivitySpinner(View view) {
        final Spinner spinner = (Spinner) view.findViewById(R.id.input_activities);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                getActivity().getApplicationContext(),
                R.array.happening_activities,
                android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setSelection(0);
    }

    void updateDateTime() {
        SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd hh:mm aa");
        mDateTimeView.setText(df.format(mCalendar.getTime()));
    }

    public void submit(Happening happening) {
        EditText description = (EditText) getActivity().findViewById(R.id.input_description);
        Spinner activity = (Spinner)getActivity().findViewById(R.id.input_activities);

        happening.setDescription(description.getText().toString());
        happening.setActivity(activity.getSelectedItem().toString());
        happening.setDateTime(mCalendar.getTime());
    }
}
