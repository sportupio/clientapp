package io.sportup.android.net;

import android.util.Log;

import com.loopj.android.http.JsonHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONObject;

public abstract class JsonResponseHandler extends JsonHttpResponseHandler {

    private static final String TAG = "JsonResponseHandler";

    public abstract void success(int status, JSONObject response);

    public abstract void failure(int status, JSONObject response);

    @Override
    public void onSuccess(int status, Header[] headers, JSONObject response) {
        Log.d(TAG, "SUCCESS JSON - Status returned: " + status + ";" + response);
        success(status, response);
    }

    @Override
    public void onSuccess(int status, Header[] headers, String responseString) {
        Log.d(TAG, "SUCCESS String - Status returned: " + status + ";" + responseString);
        success(status, null);
    }

    @Override
    public void onFailure(int status, Header[] headers, Throwable throwable, JSONObject errorResponse) {
        Log.i(TAG, "FAILURE JSON - Status returned: " + status);
        failure(status, errorResponse);
    }

    @Override
    public void onFailure(int status, Header[] headers, String responseString, Throwable throwable) {
        Log.i(TAG, "FAILURE String - Status returned: " + status);
        failure(status, null);
    }
}
