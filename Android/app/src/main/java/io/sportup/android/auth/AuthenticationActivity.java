package io.sportup.android.auth;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import io.sportup.android.sportup.SportupActivity;
import io.sportup.android.net.ApiClient;
import io.sportup.android.Application;
import io.sportup.android.R;
import io.sportup.android.net.JsonResponseHandler;

public class AuthenticationActivity extends FragmentActivity
        implements TermsAndConditions.TermsAndConditionsListener,
            FragmentManager.OnBackStackChangedListener {

    private static final String TAG = "Authenticate";
    private ApiClient mApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActionBar().hide();
        mApi = ((Application) getApplication()).api();
        getSupportFragmentManager().addOnBackStackChangedListener(this);
        setContentView(R.layout.auth_activity);
    }

    @Override
    public void onBackPressed() {
        // if there is a fragment and the back stack of this fragment is not empty,
        // then emulate 'onBackPressed' behaviour, because in default, it is not working
        FragmentManager fm = getSupportFragmentManager();
        if(fm != null) {
            if(fm.getBackStackEntryCount() > 0){
                fm.popBackStack();
                return;
            }
        }
        super.onBackPressed();
    }

    @Override
    public boolean onNavigateUp() {
        FragmentManager fm = getSupportFragmentManager();
        if(fm != null) {
            if(fm.getBackStackEntryCount() > 0){
                fm.popBackStack();
            }
        }
        return true;
    }

    @Override
    public void onBackStackChanged() {
        if(getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getActionBar().show();
        } else {
            getActionBar().hide();
        }
    }

    public void onSignInClick(View view) {
        Log.d(TAG, "SignIn clicked");

        SignIn fragment = new SignIn();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_up_1,R.anim.no_change, R.anim.no_change, R.anim.slide_down_2);
        transaction.add(R.id.auth_main_fragment, fragment);
        transaction.addToBackStack("SignIn");
        transaction.commit();
    }

    public void onHelpAccountClick(View view) {
        Log.d(TAG, "HelpAccount clicked");

        HelpAccount fragment = new HelpAccount();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_up_1,R.anim.no_change, R.anim.no_change, R.anim.slide_down_2);
        transaction.add(R.id.auth_main_fragment, fragment);
        transaction.addToBackStack("HelpAccount");
        transaction.commit();
    }

    public void onRegisterClick(View view) {
        Log.d(TAG, "Register clicked");

        TermsAndConditions tac = new TermsAndConditions();
        tac.show(getSupportFragmentManager(), "TermsAndConditions");
    }

    public void onResetPasswordClick(View view) {
        Log.d(TAG, "ResetPassword clicked");

        ResetPassword fragment = new ResetPassword();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_up_1,R.anim.no_change, R.anim.no_change, R.anim.slide_down_2);
        transaction.add(R.id.auth_main_fragment, fragment);
        transaction.addToBackStack("ResetPassword");
        transaction.commit();
    }

    public void onDeleteAccountClick(View view) {
        Log.d(TAG, "DeleteAccount clicked");

        DeleteAccount fragment = new DeleteAccount();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_up_1,R.anim.no_change, R.anim.no_change, R.anim.slide_down_2);
        transaction.add(R.id.auth_main_fragment, fragment);
        transaction.addToBackStack("DeleteAccount");
        transaction.commit();
    }

    public void onChangePasswordClick(View view) {
        Log.d(TAG, "ChangePassword clicked");

        ChangePassword fragment = new ChangePassword();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_up_1,R.anim.no_change, R.anim.no_change, R.anim.slide_down_2);
        transaction.add(R.id.auth_main_fragment, fragment);
        transaction.addToBackStack("ChangePassword");
        transaction.commit();
    }

    public void onRegisterWithEmailClick(View view) {
        Log.d(TAG, "Register with email clicked");

        RegisterEmail fragment = new RegisterEmail();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_up_1,R.anim.no_change, R.anim.no_change, R.anim.slide_down_2);
        transaction.add(R.id.auth_main_fragment, fragment);
        transaction.addToBackStack("RegisterEmail");
        transaction.commit();
    }

    public void onSignInSubmitClick(View view) throws Exception {
        EditText email = (EditText)findViewById(R.id.input_email);
        EditText password = (EditText)findViewById(R.id.input_password);

        mApi.signIn(email.getText().toString(), password.getText().toString(), new JsonResponseHandler() {
            @Override
            public void success(int status, JSONObject response) {
                Log.d(TAG, "Success on signIn attempt: " + response);
                try {
                    mApi.setToken(response.getString("token"));
                    finishAuthentication();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(int status, JSONObject response) {
                Log.w(TAG, "Failure on login attempt: " + status + ", " + response);
                String message = "Login failed";
                if(response != null) {
                    try {
                        message = response.getString("message");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                // Print message on screen, wait for next login attempt
                TextView msgBox = (TextView)findViewById(R.id.output_message);
                msgBox.setText(message);
            }
        });
    }

    public void onRegisterWithEmailSubmitClick(View view) throws Exception {
        EditText email = (EditText)findViewById(R.id.input_email);
        EditText password = (EditText)findViewById(R.id.input_password);
        EditText password_retype = (EditText)findViewById(R.id.input_password_retype);

        if(password.getText().toString().equals(password_retype.getText().toString())) {
            mApi.register(email.getText().toString(),
                    password.getText().toString(), new JsonResponseHandler() {
                @Override
                public void success(int status, JSONObject response) {
                    Log.d(TAG, "Success on registration attempt: " + response);
                    try {
                        mApi.setToken(response.getString("token"));
                        finishAuthentication();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void failure(int status, JSONObject response) {
                    Log.w(TAG, "Failure on registration attempt: " + status + ", " + response);
                    String message = "Registration failed";
                    if(response != null) {
                        try {
                            message = response.getString("message");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    // Print message on screen, wait for next login attempt
                    TextView msgBox = (TextView)findViewById(R.id.output_message);
                    msgBox.setText(message);
                }
            });
        } else {
            Log.w(TAG, "Typed passwords don't match each other: "
                    + password.getText().toString() + " vs " + password_retype.getText().toString());
            TextView msgBox = (TextView)findViewById(R.id.output_message);
            msgBox.setText("Given passwords don't match");
        }
    }

    @Override
    public void onTermsAndConditionsAccept() {
        Log.d(TAG, "Terms and conditions accepted");

        RegisterChoice registerChoiceFragment = new RegisterChoice();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.no_change,R.anim.no_change, R.anim.no_change, R.anim.slide_down_2);
        transaction.add(R.id.auth_main_fragment, registerChoiceFragment);
        transaction.addToBackStack("Register");
        transaction.commit();
    }

    private void finishAuthentication() {
        FragmentManager fm = getSupportFragmentManager();
        fm.popBackStackImmediate(fm.getBackStackEntryAt(0).getId(),
                FragmentManager.POP_BACK_STACK_INCLUSIVE);
        Intent intent = new Intent(getApplicationContext(), SportupActivity.class);
        startActivity(intent);
    }
}
