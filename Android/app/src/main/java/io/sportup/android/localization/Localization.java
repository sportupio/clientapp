package io.sportup.android.localization;

import com.mapbox.mapboxsdk.api.ILatLng;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

import io.sportup.android.utils.GeoUtils;

/**
 * Created by Konrad on 2015-05-04.
 */
public class Localization {

    private static final String TAG = "Localization";

    private ILatLng mPosition;
    private double mRadius;

    public void setPosition(ILatLng position) {
        mPosition = position;
    }

    public void setRadius(double radius) {
        mRadius = radius;
    }

    public JSONObject toJson() {
        JSONObject data = new JSONObject();
        try {
            data.put("radius", mRadius);
            data.put("loc", GeoUtils.createLoc(mPosition));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return data;
    }
}
