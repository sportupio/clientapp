package io.sportup.android.utils;

import android.content.res.Resources;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.style.StyleSpan;
import android.text.style.TypefaceSpan;
import android.util.Log;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import io.sportup.android.R;

class IcomoonTypefaceSpan extends TypefaceSpan {

    private final Typeface mTypeface;

    public IcomoonTypefaceSpan(Typeface type) {
        super("");
        mTypeface = type;
    }

    @Override
    public void updateDrawState(TextPaint ds) {
        applyCustomTypeFace(ds, mTypeface);
    }

    @Override
    public void updateMeasureState(TextPaint paint) {
        applyCustomTypeFace(paint, mTypeface);
    }

    private static void applyCustomTypeFace(Paint paint, Typeface tf) {
        int oldStyle;
        Typeface old = paint.getTypeface();
        if (old == null) {
            oldStyle = 0;
        } else {
            oldStyle = old.getStyle();
        }

        int fake = oldStyle & ~tf.getStyle();
        if ((fake & Typeface.BOLD) != 0) {
            paint.setFakeBoldText(true);
        }

        if ((fake & Typeface.ITALIC) != 0) {
            paint.setTextSkewX(-0.25f);
        }
        paint.setTypeface(tf);
    }
}

public class Icomoon {

    private static final String TAG = "utils.Icomoon";

    private static Typeface mTypeface;
    private static IcomoonTypefaceSpan mSpan;
    private static Resources mResources;
    private static HashMap<String, Integer> mIconMap = new HashMap<String, Integer>();

    public static void init(Resources resources) {
        mTypeface = Typeface.createFromAsset(resources.getAssets(), "icomoon.ttf");
        mSpan = new IcomoonTypefaceSpan(mTypeface);

        mResources = resources;

        try {
            JSONObject jsonMapping = new JSONObject(resources.getString(R.string.icomoon_mapping));
            Iterator<String> keyIt = jsonMapping.keys();
            while(keyIt.hasNext()) {
                String key = keyIt.next();
                mIconMap.put("{" + key + "}", jsonMapping.getInt(key));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void set(TextView tv, int resId) {
        tv.setTypeface(mTypeface);
        tv.setText(mResources.getString(resId));
    }

    public static void set(TextView tv, String text) {
        tv.setTypeface(mTypeface);
        tv.setText(text);
    }

    public static void set(TextView tv) {
        List<Integer> icoList = parse(tv);
        SpannableString styledString = new SpannableString(tv.getText());
        for(int ico: icoList) {
            styledString.setSpan(mSpan, ico, ico+1, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        }
        tv.setText(styledString);
    }

    private static List<Integer> parse(TextView tv) {
        String text = tv.getText().toString();
        List<Integer> result = new ArrayList<Integer>();
        int pos = 0;
        while((pos = text.indexOf('{', pos)) != -1) {
            int bracket = text.indexOf('}', pos);
            if(bracket > 0) {
                String key = text.substring(pos, bracket + 1);
                Integer value = mIconMap.get(key);
                if(value != null) {
                    text = text.replace(key, String.valueOf((char) value.intValue()));
                    result.add(pos);
                }
            }
            pos = bracket;
        }
        tv.setText(text);
        return result;
    }
}
