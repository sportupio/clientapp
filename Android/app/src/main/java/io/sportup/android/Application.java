package io.sportup.android;

import android.net.Uri;

import com.github.nkzawa.socketio.client.Socket;
import com.github.nkzawa.socketio.client.IO;
import com.mapbox.mapboxsdk.tileprovider.tilesource.MapboxTileLayer;

import java.net.URISyntaxException;

import io.sportup.android.net.ApiClient;
import io.sportup.android.utils.Icomoon;

public class Application extends android.app.Application {

    private ApiClient mApi;
    private Socket mSocket;
    private MapboxTileLayer mTileLayer;

    public void onCreate() {
        super.onCreate();
        mApi = new ApiClient(getResources());
        Icomoon.init(getResources());
    }

    public ApiClient api() {
        return mApi;
    }

    public Socket socket() { return mSocket; }

    public MapboxTileLayer tileLayer() { return mTileLayer; }

    public Socket initializeSocket() {
        try {
            Uri.Builder ub = new Uri.Builder();
            ub.scheme(getString(R.string.api_scheme))
                    .authority(getString(R.string.api_host))
                    .appendQueryParameter("token", mApi.token());
            mSocket = IO.socket(ub.build().toString());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return mSocket;
    }

    public void initializeMaps() {
        mTileLayer = new MapboxTileLayer("sportup.lae69ohh");
    }
}
