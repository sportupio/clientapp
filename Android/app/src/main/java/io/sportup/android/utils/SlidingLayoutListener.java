package io.sportup.android.utils;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by Konrad on 2015-05-03.
 */
public class SlidingLayoutListener implements View.OnTouchListener {

    private static final String TAG = "SlidingFragmentListener";

    private View mHandle;
    private View mLayout;
    private boolean mAnimating;
    final int mScreenWidth;
    
    private float mHandleDx;
    private float mLayoutDx;
    private float mX;
    private float mStartX;

    public SlidingLayoutListener(View handle, View layout) {
        mHandle = handle;
        mLayout = layout;
        mAnimating = false;
        mScreenWidth = handle.getContext().getResources().getSystem().getDisplayMetrics().widthPixels;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {

        if (mAnimating) {
            return false;
        }
        final int action = event.getAction();
        mX = event.getRawX();
        if (action == MotionEvent.ACTION_DOWN) {
            mHandleDx = mHandle.getX() - mX;
            mLayoutDx = mLayout.getX() - mX;
            mStartX = mX;
        } else if (action == MotionEvent.ACTION_UP) {
            long duration = event.getEventTime() - event.getDownTime();
            float dx = Math.abs(mX - mStartX);
            Log.d(TAG, "duration: " + duration + " dx: " + dx);
            return checkSwipe(duration, dx) || checkClick(duration, dx);
        } else {
            mHandle.setTranslationX(mX + mHandleDx);
            mLayout.setTranslationX(mX + mLayoutDx);
        }
        return true;
    }

    private boolean checkSwipe(long duration, float dx) {
        if ((duration < 600) && (dx > mScreenWidth / 5)) {
            if (mX > mStartX) {
                onSwipeRight();
            } else {
                onSwipeLeft();
            }
            return true;
        }
        return false;
    }

    private boolean checkClick(long duration, float dx) {
        Log.d(TAG, "Check click");
        boolean isClick = (duration < 600) && (dx < mScreenWidth / 10);
        if (mX > mScreenWidth / 2) {
            if (isClick) {
                onSwipeLeft();
            } else {
                onSwipeRight();
            }
        } else {
            if (isClick) {
                onSwipeRight();
            } else {
                onSwipeLeft();
            }
        }
        return true;
    }

    private void onSwipeRight() {
        ObjectAnimator btnAnimation = ObjectAnimator.ofFloat((View) mHandle, "translationX",
                mX + mHandleDx, mScreenWidth - (mHandle.getWidth() / 2));
        ObjectAnimator mapAnimation = ObjectAnimator.ofFloat((View) mLayout, "translationX",
                mX + mLayoutDx, 0);
        animate(btnAnimation, mapAnimation);
    }

    private void onSwipeLeft() {
        ObjectAnimator btnAnimation = ObjectAnimator.ofFloat((View) mHandle, "translationX",
                mX + mHandleDx, -(mHandle.getWidth() / 2));
        ObjectAnimator mapAnimation = ObjectAnimator.ofFloat((View) mLayout, "translationX",
                mX + mLayoutDx, -mScreenWidth);
        animate(btnAnimation, mapAnimation);
    }

    private void animate(ObjectAnimator btnAnimation, ObjectAnimator mapAnimation) {
        btnAnimation.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                mAnimating = true;
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                mAnimating = false;
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }
        });
        btnAnimation.start();
        mapAnimation.start();
    }
}
