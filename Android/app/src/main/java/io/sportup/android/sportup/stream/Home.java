package io.sportup.android.sportup.stream;

import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import io.sportup.android.R;

public class Home extends Fragment {

    private static final String TAG = "stream.Home";

    public Home() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.sportup_stream_home, container, false);
    }

}
