package io.sportup.android.net;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestHandle;
import com.loopj.android.http.RequestParams;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import io.sportup.android.R;
import io.sportup.android.happening.Happening;
import io.sportup.android.localization.Localization;

public class ApiClient extends AsyncHttpClient {

    private static final String TAG = "ApiClient";

    private String mToken;
    private Resources mResources;

    public ApiClient(Resources resources) {
        mResources = resources;
    }

    public void setToken(String token) {
        mToken = token;
        Log.d(TAG, "Token set to: '" + mToken + "'");
    }

    public String token() {
        return mToken;
    }

    public void clearToken() {
        mToken = null;
    }

    public void signIn(String email, String password, JsonHttpResponseHandler responseHandler) {
        JSONObject data = new JSONObject();
        try {
            data.put("email", email);
            data.put("password", password);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        post(null, data, getString(R.string.api_auth_local), responseHandler);
    }


    public void register(String email, String password, JsonHttpResponseHandler responseHandler) {
        JSONObject data = new JSONObject();
        try {
            data.put("email", email);
            data.put("password", password);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        post(null, data, getString(R.string.api_auth_register), responseHandler);
    }

    public void createHappening(Happening happening, JsonHttpResponseHandler responseHandler) {
        JSONObject data = happening.toJson();
        Log.d("TAG", "createHappening: " + data);
        post(null, data, getString(R.string.api_happenings), responseHandler);
    }

    public void createLocalization(Localization localization, JsonHttpResponseHandler responseHandler) {
        JSONObject data = localization.toJson();
        Log.d("TAG", "createLocalization: " + data);
        post(null, data, getString(R.string.api_users_areas), responseHandler);
    }

    public RequestHandle post(Context context,
                              JSONObject data,
                              String path,
                              JsonHttpResponseHandler responseHandler) {
        HttpPost request = new HttpPost(createApiUri(path).normalize());
        return sendJsonRequest(context, data, request, responseHandler);
    }

    public RequestHandle get(Context context,
                             JSONObject data,
                             String path,
                             JsonHttpResponseHandler responseHandler) {
        HttpEntityEnclosingRequestBase request = new HttpEntityEnclosingRequestBase() {
            @Override
            public String getMethod() {
                return "GET";
            }
        };
        request.setURI(createApiUri(path).normalize());
        return sendJsonRequest(context, data, request, responseHandler);
    }

    public URI hostUri() {
        try{
            URI uri = new URI(getString(R.string.api_scheme),
                    null,
                    getString(R.string.api_host),
                    -1,
                    null,
                    null,
                    null);

            Log.d(TAG, "Computed URI: " + uri.toString());
            return uri;
        } catch (URISyntaxException e) {
            Log.e(TAG, "Problem during URI calculation: " + e.toString());
        }
        return null;
    }

    private URI createApiUri(String path) {
        try{
            URI uri = new URI(getString(R.string.api_scheme),
                    null,
                    getString(R.string.api_host),
                    mResources.getInteger(R.integer.api_port),
                    path,
                    null,
                    null);

            Log.d(TAG, "Computed URI: " + uri.toString());
            return uri;
        } catch (URISyntaxException e) {
            Log.e(TAG, "Problem during URI calculation: " + e.toString());
        }
        return null;
    }

    private RequestHandle sendJsonRequest(Context context,
                                          JSONObject data,
                                          HttpEntityEnclosingRequestBase request,
                                          JsonHttpResponseHandler responseHandler) {
        if(data != null) {
            HttpEntity entity = null;
            try {
                entity = new StringEntity(data.toString());
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            request.setEntity(entity);
        }
        if(mToken != null) {
            request.setHeader("Authorization", "Bearer " + mToken);
        }

        return sendRequest((org.apache.http.impl.client.DefaultHttpClient) getHttpClient(),
                getHttpContext(),
                request,
                RequestParams.APPLICATION_JSON,
                responseHandler,
                context);
    }

    private String getString(int resourceId) {
        return mResources.getString(resourceId);
    }
}
