package io.sportup.android.map;

import android.app.Fragment;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.mapbox.mapboxsdk.tileprovider.tilesource.MapboxTileLayer;
import com.mapbox.mapboxsdk.views.MapView;

import org.json.JSONObject;

import java.sql.Time;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import io.sportup.android.R;
import io.sportup.android.net.JsonResponseHandler;
import io.sportup.android.net.MapboxClient;
import io.sportup.android.utils.Icomoon;

public class BasicFragment extends Fragment {

    private static final String TAG = "map.BasicFragment";

    protected MapboxClient mApi;
    protected MapView mMapView;
    protected Button mZoomIn;
    protected Button mZoomOut;
    protected ArrayAdapter<CharSequence> mSearchResults;

    public BasicFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.map_basic_fragment, container, false);

        mApi = new MapboxClient(getResources());

        mMapView = new MapView(getActivity().getApplicationContext());
        mMapView.setAccessToken(getString(R.string.mapbox_api_token));
        mMapView.setTileSource(new MapboxTileLayer(getString(R.string.mapbox_map_id)));

        FrameLayout mapLayout = (FrameLayout)view.findViewById(R.id.map_container);
        mapLayout.addView(mMapView);

        setupButtons(view);
        setupSearch(view);

        return view;
    }

    private void setupButtons(View view) {
        mZoomIn = (Button)view.findViewById(R.id.button_zoom_in);
        mZoomOut = (Button)view.findViewById(R.id.button_zoom_out);

        Icomoon.set((TextView)view.findViewById(R.id.button_gps), R.string.icomoon_my_location);
        Icomoon.set((TextView) mZoomIn, R.string.icomoon_plus);
        Icomoon.set((TextView) mZoomOut, R.string.icomoon_minus);

        mZoomIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMapView.zoomIn();
            }
        });
        mZoomOut.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mMapView.zoomOut();
            }
        });
    }

    private void setupSearch(View view) {
        Icomoon.set((TextView) view.findViewById(R.id.icon_search), R.string.icomoon_search);
        mSearchResults = new ArrayAdapter<CharSequence>(getActivity().getApplicationContext(),
                android.R.layout.simple_list_item_1);
        final ListView searchResult = (ListView)view.findViewById(R.id.list_search_results);
        searchResult.setAdapter(mSearchResults);

        final EditText search = (EditText)view.findViewById(R.id.input_search);

        search.addTextChangedListener(new TextWatcher() {
            private Handler mHandler = new Handler();
            private Runnable mSearchTask = new Runnable() {
                @Override
                public void run() {
                    mSearchResults.clear();
                    mApi.places(search.getText().toString(), new MapboxClient.PlaceListHandler() {
                        @Override
                        public void handle(List<Place> places) {
                            mSearchResults.addAll(places);
                        }
                    });
                }
            };

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mHandler.removeCallbacks(mSearchTask);
                if(s.length() > 0) {
                    mHandler.postDelayed(mSearchTask, 500);
                } else {
                    mSearchResults.clear();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void afterTextChanged(Editable s) {}
        });

        searchResult.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Place place = (Place) mSearchResults.getItem(position);
                mMapView.setCenter(place.mLoc);
                search.setText(null);
                mSearchResults.clear();
            }
        });
    }
}
