package io.sportup.android.localization;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import io.sportup.android.R;
import io.sportup.android.utils.Icomoon;

public class Form extends Fragment {

    private static final String TAG = "localization.Form";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.localization_form_fragment, container, false);

        Icomoon.set((TextView)view.findViewById(R.id.button_languages));
        Icomoon.set((TextView)view.findViewById(R.id.button_activities));
        return view;
    }

    public void submit(Localization localization) {

    }

}
