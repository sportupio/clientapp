package io.sportup.android.happening;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import org.json.JSONObject;

import io.sportup.android.Application;
import io.sportup.android.R;
import io.sportup.android.net.ApiClient;
import io.sportup.android.net.JsonResponseHandler;
import io.sportup.android.utils.SlidingLayoutListener;

public class CreateActivity extends Activity {

    private static final String TAG = "hap.CreateActivity";
    private ApiClient mApi;
    private Map mMap;
    private Form mForm;

    private Happening mHappening;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.happening_create_activity);

        mApi = ((Application) getApplication()).api();
        mMap = (Map)getFragmentManager().findFragmentById(R.id.map_fragment);
        mForm = (Form)getFragmentManager().findFragmentById(R.id.form_fragment);

        LinearLayout mapLayout = (LinearLayout)findViewById(R.id.map_fragment_layout);
        Button mapButton = (Button)findViewById(R.id.button_map);

        mapLayout.setTranslationX(-getResources().getDisplayMetrics().widthPixels);
        mapButton.setOnTouchListener(new SlidingLayoutListener(findViewById(R.id.button_map_layout), mapLayout));
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        View submitButton = findViewById(R.id.button_create_happening);
        LinearLayout mapButtonLayout = (LinearLayout)findViewById(R.id.button_map_layout);
        LinearLayout mapLayout = (LinearLayout)findViewById(R.id.map_fragment_layout);
        final int width = getResources().getDisplayMetrics().widthPixels;

        if(mapLayout.getTranslationX() < - (width / 2)) {
            mapButtonLayout.setTranslationX(-(mapButtonLayout.getWidth() / 2));
        } else {
            mapButtonLayout.setTranslationX(width - (mapButtonLayout.getWidth() / 2));
        }

        mapButtonLayout.setTranslationY(submitButton.getTop() - mapButtonLayout.getHeight() - 15);
    }

    public void onCreateHappeningClicked(View view) {
        ApiClient api = ((Application) getApplication()).api();
        Happening happening = new Happening();
        mForm.submit(happening);
        mMap.submit(happening);

        api.createHappening(happening,
                new JsonResponseHandler() {
                    @Override
                    public void success(int status, JSONObject response) {
                        Log.d(TAG, "Success: " + response.toString());
                        finish();
                    }

                    @Override
                    public void failure(int status, JSONObject response) {
                        Log.d(TAG, "Failure: " + response);
                    }
                });
    }
}
