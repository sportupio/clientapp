package io.sportup.android.map;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.RectF;
import android.util.Log;

import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.overlay.Overlay;
import com.mapbox.mapboxsdk.views.MapView;
import com.mapbox.mapboxsdk.views.util.Projection;

public class CircleOverlay extends Overlay {

    private static final String TAG = "CircleOverlay";

    private MapView mMapView;

    private double mRadius;
    private LatLng mCenter;

    protected Paint mPaint = new Paint();

    private final PointF mProjectedCenter = new PointF(0,0);
    private float mProjectedRadius = 0.0f;

    private final RectF mLocationRect = new RectF(0, 0, 0, 0);
    private final RectF mLocationPreviousRect = new RectF(0, 0, 0, 0);

    public CircleOverlay(double radius, LatLng center) {
        mRadius = radius;
        mCenter = center;
        mPaint.setStyle(Paint.Style.FILL);
        mPaint.setARGB(50,255,50,50);
        mPaint.setAntiAlias(true);
    }

    public CircleOverlay addTo(MapView mv) {
        mMapView = mv;
        return this;
    }

    public void setRadius(double radius) {
        mRadius = radius;
        invalidate();
    }

    public void setCenter(LatLng center) {
        mCenter = center;
        invalidate();
    }

    public double radius() { return mRadius; }

    @Override
    protected void draw(Canvas canvas, MapView mapView, boolean b) {
        if(b) {
            return;
        }
        updateDrawingPosition();
        canvas.drawCircle(mProjectedCenter.x, mProjectedCenter.y, mProjectedRadius, mPaint);
    }

    private void updateDrawingPosition() {
        if (mMapView == null) {
            return; //not on map yet
        }
        final Projection pj = mMapView.getProjection();
        pj.toMapPixels(mCenter, mProjectedCenter);
        // resolution in meter per pixel
        final float resolution = (float)pj.groundResolution(mCenter.getLatitude());
        mProjectedRadius = (float)mRadius * 1000 / resolution;
        mLocationRect.set(mProjectedCenter.x - mProjectedRadius,
                mProjectedCenter.y - mProjectedRadius,
                mProjectedCenter.x + mProjectedRadius,
                mProjectedCenter.y + mProjectedRadius);
    }

    public void invalidate() {
        if (mMapView == null) {
            return; //not on map yet
        }
        // Get new drawing bounds
        mLocationPreviousRect.set(mLocationRect);
        updateDrawingPosition();
        final RectF newRect = new RectF(mLocationRect);
        // If we had a previous location, merge in those bounds too
        newRect.union(mLocationPreviousRect);
        // Invalidate the bounds
        mMapView.post(new Runnable() {
            @Override
            public void run() {
                mMapView.invalidateMapCoordinates(newRect);
            }
        });
    }
}
